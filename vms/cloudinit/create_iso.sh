#!/bin/sh

for i in ue epdg pgw ims ; do
    cd $i
    genisoimage -output seed.iso -volid cidata -joliet -rock user-data meta-data network-config
    cd ..
done
