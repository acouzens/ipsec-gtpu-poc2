# osmo-epdg architecture prototype

Have 4 vms

ue --ipsec-> epdg --gtp-> pgw --ethernet-> ims

Current setup.
- ipsec with shared psk
- static gtp tunnels between epdg and pgw
- ims only have a icmp/http running

## how to use

Run the setup.sh scripts on the vms after the vms has been setup with the correct ips

## Does SWu works?

Not yet

## Does strongswan as ue works?

Yes.

## SWu

Logs on strongswan with SWu:
```
Jul 24 18:10:48 epdg charon-systemd[5774]: parsed IKE_AUTH request 1 [ IDi IDr CPRQ(ADDR DNS ADDR6 DNS6 PCSCF4 PCSCF6) SA TSi TSr N(EAP_ONLY) ]
Jul 24 18:10:48 epdg charon-systemd[5774]: looking for peer configs matching 192.168.0.2[internet]...192.168.56.51[0262421234567890@nai.epc.mnc42.mcc999.3gppnetwork.org]


... no matching peer config
```

But Logs with strongswan as UE:
```
Jul 24 18:17:01 epdg charon-systemd[5774]: parsed IKE_AUTH request 1 [ IDi N(INIT_CONTACT) CERTREQ IDr CPRQ(ADDR DNS) SA TSi TSr N(MOBIKE_SUP) N(ADD_4_ADDR) N(MULT_AUTH) N(EAP_ONLY) N(MSG_ID_SYN_SUP) ]

Jul 24 18:17:01 epdg charon-systemd[5774]: local endpoint changed from 192.168.0.2[500] to 192.168.0.2[4500]
Jul 24 18:17:01 epdg charon-systemd[5774]: remote endpoint changed from 192.168.0.1[500] to 192.168.0.1[4500]
Jul 24 18:17:01 epdg charon-systemd[5774]: received cert request for "C=CH, O=strongSwan, CN=strongSwan Root CA"
Jul 24 18:17:01 epdg charon-systemd[5774]: looking for peer configs matching 192.168.0.2[epdg]...192.168.0.1[0999421234567890@wlan.mnc999.mcc42.3gppnetwork.org]
Jul 24 18:17:01 epdg charon-systemd[5774]: peer config "rw", ike match: 1052 (192.168.0.2...%any IKEv2)
Jul 24 18:17:01 epdg charon-systemd[5774]:   local id match: 20 (ID_FQDN: 65:70:64:67)
Jul 24 18:17:01 epdg charon-systemd[5774]:   remote id match: 1 (ID_RFC822_ADDR: 30:39:39:39:34:32:31:32:33:34:35:36:37:38:39:30:40:77:6c:61:6e:2e:6d:6e:63:39:39:39:2e:6d:63:63:34:32:2e:33:67:70:70:6e:65:74:77:6f:72:6b:2e:6f:72:67)
Jul 24 18:17:01 epdg charon-systemd[5774]:   candidate "rw", match: 20/1/1052 (me/other/ike)
Jul 24 18:17:01 epdg charon-systemd[5774]: selected peer config 'rw'
Jul 24 18:17:01 epdg charon-systemd[5774]: initiating EAP_AKA method (id 0x5D)


```
