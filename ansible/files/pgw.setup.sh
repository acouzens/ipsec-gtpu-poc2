#!/bin/sh

term() {
    kill %1
    gtp-link del gtp
    exit 0
}

trap term INT

echo 1 > /proc/sys/net/ipv4/ip_forward
modprobe gtp
echo -n 'module gtp +p' > /sys/kernel/debug/dynamic_debug/control
gtp-link del gtp 2>/dev/null
gtp-link add gtp &
sleep 2

ip route a 172.20.0.0/24 dev gtp
gtp-tunnel add gtp v1 100 100 172.20.0.0 10.0.0.1
gtp-tunnel add gtp v1 101 101 172.20.0.1 10.0.0.1
gtp-tunnel add gtp v1 102 102 172.20.0.2 10.0.0.1

echo "Waiting for CTRL-C"
wait %1


