#!/bin/sh

term() {
    kill %1
    gtp-link del gtp
    exit 0
}

trap term INT

echo 1 > /proc/sys/net/ipv4/ip_forward
modprobe gtp
echo -n 'module gtp +p' > /sys/kernel/debug/dynamic_debug/control
gtp-link del gtp 2>/dev/null
gtp-link add gtp --sgsn &
sleep 2

# only for testing without ue vm
# ip a a 172.20.0.2/32 dev lo
# ip r a 172.20.0.0/24 dev gtp

ip rule add from 172.20.0.0/24 lookup 221 priority 100
ip rule add from 172.20.0.0/24 blackhole priority 101
ip route add default dev gtp table 221

gtp-tunnel add gtp v1 100 100 172.20.0.0 10.0.0.2
gtp-tunnel add gtp v1 101 101 172.20.0.1 10.0.0.2
gtp-tunnel add gtp v1 102 102 172.20.0.2 10.0.0.2

echo "Waiting for CTRL-C"
wait %1
