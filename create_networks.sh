#!/bin/sh

for br in ipsec gtp plain ; do
    brctl addbr $br
    sleep 1
    ip link set $br up
done

